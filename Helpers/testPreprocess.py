import nltk.data
import pickle

File_object = open(r"dataset_raw/articles_everything_pickled","rb")
data_raw = pickle.load(File_object)
File_object.close()

chars_s = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZČčŠšŽžÐßäöÖÄđćäåæçëïńößşü !?.,1234567890%'
chars = []
for l in chars_s:
	chars.append(l)

print(len(data_raw))

dic = {}

tokenizer = nltk.data.load('tokenizers/punkt/slovene.pickle')

preProcessed = []
for article in data_raw:
	if(len(article)>3):
		new_article = []
		for paragraph in article:

			for s in tokenizer.tokenize(paragraph):
				string = s.replace(u'\xa0', u' ')
				string = string.replace(u'\n', u'')
				for l in string:
					if(l not in chars):
						string = string.replace(l,'')

				if(string[-1] not in ['!','.','?']):
					string = string+'.'

				if(string[0]!=' '):
					string = ' '+string


				if(len(string)>0):

					#dic[string] = 1
					new_article.append(string)
				else:
					pass
		if(len(new_article) > 0):
			preProcessed.append(new_article)
			

for article in preProcessed:
	print('\n\n')
	print(article)
