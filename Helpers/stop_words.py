import pickle

NUMS = ['0','1','2','3','4','5','6','7','8','9']
SYMBOLS = ['.', '%']

File_object = open(r"Resource/bag_of_words_all_pickled","rb")
data_raw = pickle.load(File_object)
File_object.close()


File_object = open(r"Resource/sskj_alphabetcly_organized_pickled","rb")
sskj = pickle.load(File_object)
File_object.close()

all_dic = {}

print(len(data_raw))
alllen = int(len(data_raw)/100)
counter = 0

for dicc in data_raw:
	if(counter // alllen == 0):
		print('=', end="", flush=True)
	for dic in dicc:

		for key, value in dic.items():
			if(key[0] not in SYMBOLS and key[0] not in NUMS and key in sskj[key[0].lower()]):
				if key not in all_dic:
					all_dic[key] = value
				else:
					all_dic[key] += value
	counter += 1			
stop_words = []

for key,value in sorted(all_dic.items(), key=lambda x: x[1]):
	if(counter // alllen == 0):
		print('=', end="", flush=True)
	stop_words.append(key)
	counter += 1


stop = ''
counter = 0
for sw in stop_words[-52:]:
	if counter % 10 == 0:
		stop = stop + '\n'
	stop = stop+' '+sw
	counter += 1

print(' COMPLETED')
print('\n')
print(stop)

File_object = open('Resource/stop_words_pickle', 'wb')
pickle.dump(stop_words[-52:], File_object,-1)
File_object.close()