# coding: utf-8
from __future__ import unicode_literals
from io import open
import pickle
import re
import math
import nltk.data
import copy
import torch
import torch.nn as nn
import numpy as np
import unicodedata

File_object = open(r"Helpers/dataset_raw/articles_everything_pickled","rb")
data_raw = pickle.load(File_object)
File_object.close()

'''
File_object = open(r"Helpers/Resource/sskj_alphabetcly_organized_pickled","rb")
sskj = pickle.load(File_object)
File_object.close()
'''

File_object = open(r"Helpers/Resource/stop_words_pickle","rb")
stop_words = pickle.load(File_object)
File_object.close()

File_object = open(r"Helpers/Resource/sskj_hashmap","rb")
sskj = pickle.load(File_object)
File_object.close()

preProcesed = []

big_letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZČŠŽ'
chars_s = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZČčŠšŽžÐßäöÖÄđćäåæçëïńößşü 1234567890%'
chars = []
for l in chars_s:
	chars.append(l)
nu = '1234567890'
nums = []
for n in nu:
	nums.append(n)

def create_BOW(string):
	BOW = dict()
	for l in string:
		if l not in BOW:
			BOW[l] = 1
		else:
			BOW[l] += 1
	return BOW

def get_cosine(vec1, vec2):
	intersection = set(vec1.keys()) & set(vec2.keys())
	numerator = sum([vec1[x] * vec2[x] for x in intersection])

	sum1 = sum([vec1[x]**2 for x in vec1.keys()])
	sum2 = sum([vec2[x]**2 for x in vec2.keys()])
	denominator = math.sqrt(sum1) * math.sqrt(sum2)

	if not denominator:
		return 0.0
	else:
		return float(numerator) / denominator


class CBOW(torch.nn.Module):

	def __init__(self, vocab_size, embedding_dim):
		super(CBOW, self).__init__()

		#out: 1 x emdedding_dim
		self.embeddings = nn.Embedding(vocab_size, embedding_dim)

		self.linear1 = nn.Linear(embedding_dim, 128)

		self.activation_function1 = nn.ReLU()
		
		#out: 1 x vocab_size
		self.linear2 = nn.Linear(128, vocab_size)

		self.activation_function2 = nn.LogSoftmax(dim = -1)
		

	def forward(self, inputs):
		embeds = sum(self.embeddings(inputs)).view(1,-1)
		out = self.linear1(embeds)
		out = self.activation_function1(out)
		out = self.linear2(out)
		out = self.activation_function2(out)
		return out

	def get_word_emdedding(self, word):
		word = torch.LongTensor([word_to_ix[word]])
		return self.embeddings(word).view(1,-1)

def make_context_vector(context, word_to_ix):
	idxs = [word_to_ix[w] for w in context]
	return torch.tensor(idxs, dtype=torch.long)

def get_index_of_max(input):
	index = 0
	for i in range(1, len(input)):
		if input[i] > input[index]:
			index = i 
	return index

def get_max_prob_result(input, ix_to_word):
	return ix_to_word[get_index_of_max(input)]

CONTEXT_SIZE = 2  # 2 words to the left, 2 to the right
EMDEDDING_DIM = 100

word_to_ix = {}
ix_to_word = {}

# ------------------- Parser -------------------------

dic = {}

tokenizer = nltk.data.load('tokenizers/punkt/slovene.pickle')

preProcesed = []

bar_counter = 0
len_bar = int(len(data_raw)/10)
end_bar = 10
bar = '['
print(bar+' '*end_bar+']', end="\r", flush=True)

for article in data_raw:
	new_article = []
	if(len(article)>3):
		for paragraph in article:
			for s in tokenizer.tokenize(paragraph):
				if(len(s) > 1):
					#string = unicodedata.normalize("NFKD", s)
					string = s.replace(u'\xa0', u'')
					string = string.replace(u'\n', u'')

					end = ''
					if(len(string)>1):
						if(string[-1] not in ['!','.','?']):
							#string = string+'.'
							end = '.'
						else:
							end = string[-1]

					for l in string:
						if(l not in chars):
							string = string.replace(l,' ')
							string = string.split(' ')
							string = ' '.join(string)
							string += end					

					if(len(string)>1):
						new_article.append(string)
					else:
						pass

	if(len(new_article) > 3):
		preProcesed.append(new_article)
	bar_counter += 1
	if(bar_counter % len_bar == 0):
		bar += '='
		end_bar -= 1
		print(bar+' '*end_bar+']', end="\r", flush=True)

print(bar+' '*end_bar+'] - COMPLETED', end="\n", flush=True)

# ------------------------- Feature extraction ------------------------------------------

# Occurence of every word in all of documents document
occurence_of_every_word_once_in_document = {} 
occurence_of_every_word = {}

bag_of_words_all = [] # bag of globally through all the articles
words_in_document_list =[] # number of words in every article

bag_of_words_article_list = [] # bag of words for every article

original_text = copy.deepcopy(preProcesed)
nums_in_text = copy.deepcopy(preProcesed)

bar_counter = 0
len_bar = int(len(preProcesed)/20)
end_bar = 100
bar = '['
print(bar+' '*end_bar+']', end="\r", flush=True)

corpus = []
vocab = {}
counter_article = 0
for article in preProcesed:
	counter_sentence = 0
	for sentence in article:
		nums_in_text[counter_article][counter_sentence] = 0
		end = sentence[-1]
		words = sentence[:-1].split(' ')
		new_sent = []
		org_sent = []
		for word in words:
			if(len(word)>0 and word != ' '):
				org_sent.append(word)
				if(word[0] in nums):
					nums_in_text[counter_article][counter_sentence] += 1
				else:
					if(word not in stop_words): #remove the stop words
						if(word not in vocab):
							vocab[word.lower()] = 1
						new_sent.append(word.lower())	#remove the stop words
		if(len(org_sent)>0):
			original_text[counter_article][counter_sentence] = ' '.join(org_sent) + end
		if(len(new_sent)>0):
			preProcesed[counter_article][counter_sentence] = ' '.join(new_sent)
			if(len(new_sent)>4):
				corpus.append(new_sent)
				

		counter_sentence+=1
	counter_article+=1

#------------- CBOW ----------
vocab_size = len(vocab)

for i, word in enumerate(vocab):
	word_to_ix[word] = i
	ix_to_word[i] = word

model = CBOW(vocab_size, EMDEDDING_DIM)
model.load_state_dict(torch.load("Helpers/Resource/model.torch"))
model.eval()
#------------- CBOW ----------


for article in preProcesed: # fill all the required resources like bag of words and diffrent
	# counts which are required later
	bag_of_words_article_bag = {}
	bag_of_words_article = []
	words_in_document = 0
	already_checked_words = []
	for sentence in article:
		bag_of_words = {}
		words = sentence.split(' ')
		for word in words:
			word = word.lower()
			if(word in occurence_of_every_word):
				occurence_of_every_word[word] += 1
			else:
				occurence_of_every_word[word] = 1

			if(word in occurence_of_every_word_once_in_document 
				not in already_checked_words):
				occurence_of_every_word_once_in_document[word] += 1
				already_checked_words.append(word)
			else:
				occurence_of_every_word_once_in_document[word] = 1
				already_checked_words.append(word)

			if(len(word) != 0):
				words_in_document += 1
				if(word in bag_of_words):
					bag_of_words[word]+=1
				else:
					bag_of_words[word]=1

			if(len(word) != 0):
				if(word in bag_of_words_article_bag):
					bag_of_words_article_bag[word]+=1
				else:
					bag_of_words_article_bag[word]=1

		bag_of_words_article.append(bag_of_words)
	bag_of_words_all.append(bag_of_words_article)
	words_in_document_list.append(words_in_document)
	bag_of_words_article_list.append(bag_of_words_article_bag)
	bar_counter += 1
	if(bar_counter % len_bar == 0):
		bar += '='
		end_bar -= 1
		print(bar+' '*end_bar+']', end="\r", flush=True)



# Biggest TF * IDF
BiggestTFIDF = {}
TFIDF_article =[]

counter_article = 0
for article in preProcesed:
	counter_sentence = 0
	TFIDF_D = {}
	for sentence in article:
		already_checked_words = []
		if(counter_sentence == 0):
			pass
		else:
			for word, value in bag_of_words_all[counter_article][counter_sentence].items():
				if(word not in already_checked_words):
					TF = (bag_of_words_article_list[counter_article][word]/
						words_in_document_list[counter_article])
					IDF = math.log(len(preProcesed)/
						occurence_of_every_word_once_in_document[word])
					TFIDF = TF * IDF
					TFIDF_D[word] = TFIDF
					already_checked_words.append(word)
					try:
						if(TFIDF > BiggestTFIDF[word]):
							BiggestTFIDF[word] = TFIDF
					except:
						BiggestTFIDF[word] = TFIDF

		counter_sentence += 1
	counter_article += 1
	bar_counter += 1
	if(bar_counter % len_bar == 0):
		bar += '='
		end_bar -= 1
		print(bar+' '*end_bar+']', end="\r", flush=True)
	TFIDF_article.append(TFIDF_D)

biggest_simmilarty_in_article = []
max_thematic_words_sentence_list = []
features_list = [] # all of the features
counter_article = 0
for article in preProcesed:
	num_of_sentences = math.floor((len(preProcesed[counter_article])/100)*15)
	title_lenght = len(preProcesed[counter_article][0])
	counter_sentence = 0
	max_thematic_words_sentence = 0 
	biggestSim = 0
	longest_sentence = ''
	feature_list = [] # only one of the features
	for sentence in article:
		if(counter_sentence == 0): # first sentence is the title
			pass
		else:
			feature_list.append([0])
			words = sentence.split(' ')
			
			#0. title cosine simmilarty 
			feature_list[counter_sentence-1][0] = get_cosine(bag_of_words_all[counter_article][0],
			bag_of_words_all[counter_article][counter_sentence])

			#print(bag_of_words_all[counter_article][0])
			#1. Sentence position
			if (counter_sentence > num_of_sentences):
				feature_list[counter_sentence-1].append(0)
			else:
				feature_list[counter_sentence-1].append((num_of_sentences+1-counter_sentence)/
					num_of_sentences)

			#2. Term weight
			sum1=0
			sum2=0
			for word in words:
				word = word.lower()
				if(len(word)>0 and TFIDF_article[counter_article][word]>0):
					sum1 += TFIDF_article[counter_article][word]
					sum2 += BiggestTFIDF[word]
			
			if(sum2>0):
				feature_list[counter_sentence-1].append(sum1/sum2)
			else:
				feature_list[counter_sentence-1].append(0)

			#3. Thematic words part 1
			data = []
			Thematic = 0
			if(len(sentence)>4):
				words = sentence.split()
				for i in range(2, len(words) - 2):
					context = [words[i - 2], words[i - 1],
							   words[i + 1], words[i + 2]]
					target = words[i]
					data.append((context, target))
				for c,t in data:
					try:
						context_vector = make_context_vector(c, word_to_ix)
						a = model(context_vector).data.numpy()
						res = get_max_prob_result(a[0], ix_to_word)
					except Exception as e:
						Thematic += 0.1
					Thematic += get_cosine(create_BOW(res),create_BOW(t))
				feature_list[counter_sentence-1].append(Thematic/len(sentence))
			else:
				feature_list[counter_sentence-1].append(len(sentence)/15)
					
			#sum_of_thematic_words = 0
			#already_checked_words = []	
			#for word in words:	
				#word = word.lower()
				#if(len(word)>0 and TFIDF_article[counter_article][word]>0):
					#sum_of_thematic_words += TFIDF_article[counter_article][word]
			#if(sum_of_thematic_words > max_thematic_words_sentence):
				#max_thematic_words_sentence = sum_of_thematic_words
			#feature_list[counter_sentence-1].append(sum_of_thematic_words)
			
			#4. Proper nouns
			nouns = 0
			for word in words:
				word = word.lower()
				try:
					if(word[0] and word not in sskj):
						nouns += 1
				except Exception as e:
					nouns += 1
					

			feature_list[counter_sentence-1].append(nouns/len(sentence))

			#5. Sentence lenght part 1
			if(len(sentence) > len(longest_sentence)):
				longest_sentence = sentence 
			feature_list[counter_sentence-1].append(len(longest_sentence))

			#6. Sentence to sentence simillarty part 1
			simmilarty = 0
			for sentence_tmp in range(len(article)):
				if(sentence_tmp != counter_sentence):
					simmilarty += get_cosine(bag_of_words_all[counter_article][sentence_tmp],
						bag_of_words_all[counter_article][counter_sentence])
			if(simmilarty>biggestSim):
				biggestSim = simmilarty
			feature_list[counter_sentence-1].append(simmilarty)

			#7. Numerical data
			numbers = nums_in_text[counter_article][counter_sentence]
			feature_list[counter_sentence-1].append(numbers/len(sentence))


		counter_sentence+=1
	features_list.append(feature_list)
	counter_article+=1
	max_thematic_words_sentence_list.append(max_thematic_words_sentence)
	biggest_simmilarty_in_article.append(biggestSim)
	bar_counter += 1
	if(bar_counter % len_bar == 0):
		bar += '='
		end_bar -= 1
		print(bar+' '*end_bar+']', end="\r", flush=True)


#3. Thematic words part 2, sentence lenght part 2 and sentence simmilarty part 2
counter_article = 0
for article in preProcesed:
	counter_sentence = 0
	longest_sentence = features_list[counter_article][-1][5]
	for sentence in article:
		if(counter_sentence == 0):
			pass
		else:
			#x = features_list[counter_article][counter_sentence-1][3]
			#features_list[counter_article][counter_sentence-1][3]=(x/
				#max_thematic_words_sentence_list[counter_article])

			
			features_list[counter_article][counter_sentence-1][5] = (
				len(sentence)/longest_sentence)

			sim = features_list[counter_article][counter_sentence-1][6]
			biggestSim = biggest_simmilarty_in_article[counter_article]
			if(biggestSim == 0):
				features_list[counter_article][counter_sentence-1][6] = 0
			else:
				features_list[counter_article][counter_sentence-1][6] = sim/biggestSim

		counter_sentence += 1
	counter_article += 1
	bar_counter += 1
	if(bar_counter % len_bar == 0):
		bar += '='
		end_bar -= 1
		print(bar+' '*end_bar+']', end="\r", flush=True)



File_object = open('Helpers/preprocessed_text_pickled', 'wb')
pickle.dump(original_text, File_object,-1)
File_object.close()

File_object = open('Helpers/features_pickled', 'wb')
pickle.dump(features_list, File_object,-1)
File_object.close()

File_object = open('Helpers/Resource/bag_of_words_all_pickled', 'wb')
pickle.dump(bag_of_words_all, File_object,-1)
File_object.close()

File_object = open('Helpers/Resource/occurence_of_every_word_once_in_document_pickled', 'wb')
pickle.dump(occurence_of_every_word_once_in_document, File_object,-1)
File_object.close()

File_object = open('Helpers/Resource/corpus_pickle', 'wb')
pickle.dump(corpus, File_object,-1)
File_object.close()

File_object = open('Helpers/Resource/vocab_pickle', 'wb')
pickle.dump(vocab, File_object,-1)
File_object.close()

File_object = open('Helpers/Resource/preProcessed_pickle', 'wb')
pickle.dump(preProcesed, File_object,-1)
File_object.close()

print(bar+'] - COMPLETED', end="\n", flush=True)

