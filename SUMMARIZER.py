import repo as R
import sys
import pickle
import tensorflow as tf
from pathlib import Path
import numpy as np
from math import ceil
from math import floor
import logging
import os

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'  # FATAL
logging.getLogger('tensorflow').setLevel(logging.FATAL)


#File_object = open(r"test","rb")
with open(sys.argv[1]) as f:
    text = f.readlines()
#File_object.close()

preProcessed = R.preProcess(text)
features = R.feature_extraction(preProcessed)
GSM = R.generalStatisticMethod_summary(features)
NB = R.getNaiveBayes(GSM)
fuzzied = R.fuzzification(NB)
FL,fuzzy_s = R.fuzzy_logic(fuzzied)

root = Path(__file__).absolute().parents[0]
model = tf.keras.models.load_model(str(root)+'/NN/model.h5')

summaries = open(str(root)+'/Summaries/'+preProcessed[0]+'_summ', 'w', encoding="utf8")

sentence_index = 1
summaries.write('Title: '+preProcessed[0]+'\n\n')

predictions = []
predictions_model = model.predict(np.array(FL,dtype = 'int32'))

for prediction in predictions_model:
	predictions.append((np.argmax(prediction),sentence_index))
	sentence_index+=1

predictions.sort(reverse=True)

# how many sentences included in summary
how_many_sentences_in_summary = ceil((len(preProcessed)*1.0)/100 * 30) # 20 percent
included =[]
sentence_index = 0
for tup in predictions:
	pred, sentence_position = tup
	if(sentence_index<how_many_sentences_in_summary):
		included.append(sentence_position)
	sentence_index+=1

included.sort()
for sentence_position in included:
	summaries.write(preProcessed[sentence_position])

summaries.write('\n\nOriginal:\n')
summaries.write(str(preProcessed)+' ')

summaries.close()

