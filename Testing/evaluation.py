import pickle
from pathlib import Path
import logging
import os
import numpy as np
import pandas as pd
from math import ceil
import matplotlib.pyplot as plt
import copy

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'  # FATAL
logging.getLogger('tensorflow').setLevel(logging.FATAL)
root = Path(__file__).absolute().parents[1]

File_object = open(str(root)+"/Summaries_result_vectors/general_statistical_method_pickled","rb")
gsm = pickle.load(File_object)
File_object.close()

File_object = open(str(root)+"/Summaries_result_vectors/naive_bayes_pickled","rb")
nb = pickle.load(File_object)
File_object.close()

File_object = open(str(root)+'/Summaries_result_vectors/fuzzy_logic_pickled',"rb")
fuzzy = pickle.load(File_object)
File_object.close()

File_object = open(str(root)+'/Helpers/preprocessed_text_pickled',"rb")
text = pickle.load(File_object)
File_object.close()

File_object = open('predictions_pickled',"rb")
pred = pickle.load(File_object)
File_object.close()

root = Path(__file__).absolute().parents[0]
Methods = os.listdir(path=str(root)+'/eval')
fifties = []
for method in Methods:
	File_object = open(str(root)+'/eval/'+method+'/fifty_pickle',"rb")
	fifty = pickle.load(File_object)
	File_object.close()
	fifties.append(fifty)

#print(fifties[0])

HOW_MANY_TEXTS = len(pred)
gsm = gsm[:HOW_MANY_TEXTS]
nb = nb[:HOW_MANY_TEXTS]
fuzzy_shorter = fuzzy[:HOW_MANY_TEXTS]
pred = pred[:HOW_MANY_TEXTS]
Precision_All = []
compression = []
line_GSM = 0
line_NB = 0
line_FL = 0 
line_NN = 0
comp_line_GSM = 0
comp_line_NB = 0
comp_line_FL = 0
comp_line_NN = 0

article_index = 0
for article in gsm:

	predictions = copy.deepcopy(pred[article_index])

	predictions.sort()

	
	lenght_sentence = 0
	comp_vector = []

	Precision_GSM = 0
	Precision_NB = 0
	Precision_FL = 0
	lenght_included_GSM = 0
	lenght_included_NB = 0
	lenght_included_FL = 0
	lenght_included_NN = 0

	for sentence_position, incl in predictions:

		GSM = fifties[0][article_index][sentence_position]
		NB = fifties[3][article_index][sentence_position]
		FL = fifties[1][article_index][sentence_position]
		NN = fifties[2][article_index][sentence_position]
		
		if(GSM == NN):
			Precision_GSM += 1
		else:
			Precision_GSM -= 1
		if(NB == NN):
			Precision_NB += 1
		else:
			Precision_NB -= 1
		if(FL == NN):
			Precision_FL += 1
		else:
			Precision_FL -= 1
		if(NN == 1):

			lenght_included_NN+=len(text[article_index][sentence_position+1])
		if(GSM == 1):

			lenght_included_GSM+=len(text[article_index][sentence_position+1])
		if(NB == 1):
			lenght_included_NB+=len(text[article_index][sentence_position+1])
		if(FL == 1):
			lenght_included_FL+=len(text[article_index][sentence_position+1])

		lenght_sentence += len(text[article_index][sentence_position+1])

	line_GSM += Precision_GSM/len(predictions)
	line_NB += Precision_NB/len(predictions)
	line_FL += Precision_FL/len(predictions)
	line_NN += 1

	#if Precision_All == []:
		#Precision_All = [[
			#Precision_GSM/len(predictions),
			#Precision_NB/len(predictions),
			#Precision_FL/len(predictions)]]
	#else:
		#Precision_All.append([
			#Precision_GSM/len(predictions),
			#Precision_NB/len(predictions),
			#Precision_FL/len(predictions)])

	Precision_All.append([
			line_GSM,
			line_NB,
			line_FL,
			line_NN])

	comp_line_NN += lenght_included_NN/lenght_sentence
	comp_line_GSM += lenght_included_GSM/lenght_sentence
	comp_line_NB += lenght_included_NB/lenght_sentence
	comp_line_FL += lenght_included_FL/lenght_sentence
	
	comp_vector.append(comp_line_GSM)
	comp_vector.append(comp_line_NB)
	comp_vector.append(comp_line_FL)
	comp_vector.append(comp_line_NN)

	compression.append(comp_vector)

	article_index += 1



df = pd.DataFrame(Precision_All, columns =['GSM', 'NB', 'FL','NN'], dtype = float) 

df.plot.line(rot=0)
#,color = ['black', 'yellow','blue','red']
df = pd.DataFrame(compression, columns =['compression_GSM', 'compression_NB', 'compression_FL','compression_NN'], dtype = float) 

df.plot.line(rot=0)
#,color = ['black', 'yellow','blue','red']
plt.show()