import sys
import datetime
import pickle
from pathlib import Path
import os

File_object = open('predictions_pickled',"rb")
pred = pickle.load(File_object)
File_object.close()

HOW_MANY = len(pred)

root = Path(__file__).absolute().parents[1]

Methods = os.listdir(path=str(root)+'/Summaries_result_vectors/')

for method in Methods:

	File_object = open(str(root)+'/Summaries_result_vectors/'+method,"rb")
	features_by_article = pickle.load(File_object)
	File_object.close()

	File_object = open(str(root)+"/Helpers/preprocessed_text_pickled","rb")
	preProcessed_text = pickle.load(File_object)
	File_object.close()

	time = datetime.datetime.now()

	features_by_article_shorter = features_by_article[:HOW_MANY]
	preProcessed_text_shorter = preProcessed_text[:HOW_MANY]

	summaries = open('test_summaries/'+method.split('_pickle')[0], 'w', encoding="utf8") #, encoding="utf8"

	article_index = 0
	for article in features_by_article_shorter:
		sentence_index = 1
		summaries.write('Title: '+preProcessed_text[article_index][0]+'\n\n')
		for sentence_vec in article:
			if(sentence_vec[-1] == 1):
				summaries.write(preProcessed_text[article_index][sentence_index]+' ')
			sentence_index +=1
		
		summaries.write('\n\n')
		summaries.write(str(preProcessed_text[article_index]))
		summaries.write('\n-------------------------------------------\n')
		article_index +=1

	summaries.close()

