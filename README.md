Automatic text summarization using feauter extraction and combination of methods.

This is a simple representation how process looks, general statistic method and naive bayes add their values to the sentece vectors and the new vectors are sent through the fuzzy logic, then the vectors are fed to the neural network.
![Screenshot](Representation.png)


### Usage

It has to be >+ python3.x 

* gatherResources.py runs all the pre-requsites for training neural network that includes scrapper for 24ur.si, pre-processor, general statistic method, naive bayes and fuzzy logic.
```
python3 gatherResources.py
```

* train_neural_network.py trains the nural network - tensorflow is a must
```
python3 train_neural_network.py
```
Shape of neural network is a 10 neuron input layer, 5x 50 neuron hidden layers and 10 output neuron layer, it looks like this:
![Screenshot](NeuralNetwork.png)

* SUMMARIZER.py is the main script with which we can summarize plain text files, there are some rules though: 1) Language MUST be slovenian or something close like Croatian, 2) Sentences have to seperated by commas and 3) First sentence must always be the title
```
python3 SUMMARIZER.py plain_text_file
```

Summaries are stored in the folder with the same name and are named after the titel of each article with the end '_summ'

### Testing

If you run gatherResources.py you can then test each method against each other with two testing scripts:
```
python3 test_other_methods.py
```
Generates sample texts of all three methods used
```
python3 test_neural_network.py
```
Generates sample texts with the currently trained iteration of neural network
